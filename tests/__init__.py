try:
    from trytond.modules.account_co_reports.tests.test_account_co_reports import suite  # noqa: E501
except ImportError:
    from .test_account_co_reports import suite

__all__ = ['suite']
