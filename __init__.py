from trytond.pool import Pool

from . import analytic_account_report
from . import income_statement_colgaap_report


def register():
    Pool.register(
        analytic_account_report.PrintAnalyticAccountStart,
        income_statement_colgaap_report.PrintIncomeStatementCOLGAAPStart,
        module='analytic_account_co', type_='model')
    Pool.register(
        analytic_account_report.PrintAnalyticAccount,
        income_statement_colgaap_report.PrintIncomeStatementCOLGAAP,
        module='analytic_account_co', type_='wizard')
    Pool.register(
        analytic_account_report.AnalyticAccount,
        income_statement_colgaap_report.IncomeStatementCOLGAAP,
        module='analytic_account_co', type_='report')
