# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pool import Pool


def compute_report(data):
    pool = Pool()
    Analytic_Account = pool.get('analytic_account.account')
    Analytic_Account_Line = pool.get('analytic_account.line')
    res = {}
    ctx = {
        'company': data['company']
    }

    def is_child(child, parent):
        if child in parent.childs:
            return True
        else:
            for parent_child in parent.childs:
                if child in parent_child.childs:
                    return True
                else:
                    if is_child(child, parent_child):
                        return True
        return False

    with Transaction().set_context(ctx):
        analytic_accounts_filter = Analytic_Account.search([
            ('parent', '!=', None),
            ('parent.type', '=', 'root'),
            ('id', 'in', data['analytic_account']),
        ], order=[('code', 'ASC')])

        analytic_accounts = Analytic_Account.search(
            [], order=[('code', 'ASC')])
        aas = {}
        total = 0

        for aaf in analytic_accounts_filter:
            aas[aaf.code] = {}
            aas[aaf.code][aaf.code] = [aaf, 0, 0, 0, False]

            for aa in analytic_accounts:
                record = []
                if is_child(aa, aaf):
                    record.append(aa)
                    record = record + [0, 0, 0]
                    if aa.type == 'normal':
                        aa_line = Analytic_Account_Line.search([
                            ('account', '=', aa),
                            ('date', '>=', data['start_date']),
                            ('date', '<=', data['end_date']),
                        ], order=[('date', 'ASC')])
                        lines = {}

                        for a_l in aa_line:
                            if a_l.move_line.account.code in lines:
                                lines[a_l.move_line.account.code][1] += \
                                    a_l.debit
                                lines[a_l.move_line.account.code][2] += \
                                    a_l.credit
                                lines[a_l.move_line.account.code][3].append(
                                    a_l.move_line)
                            else:
                                move_line = [a_l.move_line]
                                lines[a_l.move_line.account.code] = [
                                    a_l.move_line.account.name,
                                    a_l.debit,
                                    a_l.credit,
                                    move_line
                                    ]
                            record[1] += a_l.debit
                            record[2] += a_l.credit * -(1)
                            record[3] += a_l.debit - a_l.credit
                            total += a_l.debit - a_l.credit

                            def is_parent(parent, account, debit, credit):
                                if parent and account and parent.code in aas[
                                        aaf.code]:
                                    aas[aaf.code][parent.code][3] +=\
                                        debit - credit
                                if parent.parent and parent.parent.code:
                                    is_parent(
                                        parent.parent, account, debit, credit)

                            is_parent(aa.parent, aa, a_l.debit, a_l.credit)
                        record.append(lines)
                    aas[aaf.code][aa.code] = record
        res['record'] = aas
        res['total'] = -total

    res['root'] = Analytic_Account.search([
        ('id', '=', data['analytic_account_root'])
    ])
    res['start_date'] = data['start_date']
    res['end_date'] = data['end_date']

    res['detailed'] = data['detailed']
    res['account_move'] = data['account_move']
    res['without_balance'] = data['without_balance']

    return res


class PrintAnalyticAccountStart(ModelView):
    'Print Analytic Account'
    __name__ = 'analytic_account_co.print_analytic_account.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    detailed = fields.Boolean('Detailed')
    account_move = fields.Boolean('Account Move',
                                  states={'invisible': ~Eval('detailed'),
                                          },
                                  depends=['detailed'],
                                  help='Show account move detailed')
    without_balance = fields.Boolean('Without Balance')
    analytic_account_root = fields.Many2One(
        'analytic_account.account',
        'Analytic Account Root', required=True,
        domain=[('type', '=', 'root')]
    )
    analytic_account = fields.Many2Many(
        'analytic_account.account',
        None, None, 'Analytic Account',
        domain=[('parent', '=', Eval('analytic_account_root')),
                ('type', '!=', 'root')],
        depends=['analytic_account_root']
    )

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PrintAnalyticAccount(Wizard):
    'Print Analytic Accoount'
    __name__ = 'analytic_account_co.print_analytic_account'
    start = StateView('analytic_account_co.print_analytic_account.start',
        'analytic_account_co.print_analytic_account_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('analytic_account_co.analytic_account')

    def do_print_(self, action):
        Analytic_Account = Pool().get('analytic_account.account')

        if self.start.analytic_account:
            analytic_account_ids = [
                ac.id for ac in self.start.analytic_account]
        else:
            analytic_account = Analytic_Account.search([
                ('parent.type', '=', 'root'),
                ('parent', '=', self.start.analytic_account_root)])
            analytic_account_ids = [ac.id for ac in analytic_account]

        data = {
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'company': self.start.company.id,
            'detailed': self.start.detailed,
            'account_move': self.start.account_move,
            'without_balance': self.start.without_balance,
            'analytic_account_root': self.start.analytic_account_root.id,
            'analytic_account': analytic_account_ids
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AnalyticAccount(Report):
    __name__ = 'analytic_account_co.analytic_account'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(AnalyticAccount, cls).get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(data['company'])

        res = compute_report(data)

        report_context.update(res)
        report_context['company'] = company
        return report_context
