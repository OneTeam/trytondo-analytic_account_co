# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.pyson import Eval, PYSONEncoder
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pool import Pool


def compute_report(data, domain, codes):
    pool = Pool()
    Fiscalyear = pool.get('account.fiscalyear')
    Period = pool.get('account.period')
    Account = pool.get('account.account')
    Analytic_Account = pool.get('analytic_account.account')

    period = None
    amount_profit = 0
    res = {}

    ctx = {
        'posted': data['posted'],
    }

    def is_child(child, parent):
        if child in parent.childs:
            return True
        else:
            for parent_child in parent.childs:
                if child in parent_child.childs:
                    return True
                else:
                    if is_child(child, parent_child):
                        return True
        return False

    if data.get('period'):
        period = Period(data['period'])
        periods = Period.search([
                ('start_date', '<=', period.start_date),
        ])
        periods_ids = [period.id]
        periods_ids.extend([p.id for p in periods])
        ctx['periods'] = periods_ids
    elif data.get('periods'):
        ctx['periods'] = data.get('periods')
    else:
        ctx['fiscalyear'] = data['fiscalyear']

    view_accounts = Account.search(domain)

    reduce_ids = [a.id for a in view_accounts]

    with Transaction().set_context(ctx):
        accounts = Account.search([
            ('id', 'in', reduce_ids),
            ], order=[('code', 'ASC')]
        )

        if data.get('account_profit'):
            # account_profit = Account(data['account_profit'])
            profit_accounts = Account.search(['OR',
                ('code', 'in', ['4', '5', '6', '7']),
            ])
            amount_profit = sum([a.balance for a in profit_accounts])

        Account_Move_Line = pool.get('account.move.line')
        # analytics_accounts = []
        # records = []
        res['records'] = {}
        res['records_full'] = {}
        res['analytic_balance'] = 0

        for analytic_account_id in data['analytic_account']:
            analytic_account = Analytic_Account(analytic_account_id)
            res['records'][analytic_account.name] = {}
            result_parcial = 0
            for a in accounts:
                record = []
                record2 = []
                a.balance = a.balance * (-1)
                record.append(a)
                record2.append(a.code)
                record2.append(a.name)
                record2.append(a.balance)
                move_state = ['posted'] if data['posted'] else [
                    'posted', 'draft']
                account_move_lines = Account_Move_Line.search([
                    ('account.id', '=', a.id),
                    ('period', 'in', ctx['periods']),
                    ('move.state', 'in', move_state)
                ])
                analytic_balance = 0
                record.append([])
                record2.append([])

                if len(account_move_lines) > 0:
                    for account_move_line in account_move_lines:
                        for analytic_line in account_move_line.analytic_lines:
                            if is_child(
                                    analytic_line.account, analytic_account):
                                if analytic_line.debit > 0:
                                    analytic_balance += \
                                        analytic_line.debit * (-1)
                                analytic_balance += analytic_line.credit
                                record[1].append(analytic_line)
                            record2[3].append(analytic_line)
                    record.append(analytic_balance)
                    record2.append(analytic_balance)
                    result_parcial += analytic_balance

                    def is_parent(parent, account, debit):
                        if parent and account:
                            res['records'][analytic_account.name][
                                parent.code][2] += debit
                            res['records_full'][parent.code][4] += debit
                        if parent.parent and parent.parent.code:
                            is_parent(parent.parent, account, debit)

                    is_parent(a.parent, a.code, analytic_balance)

                else:
                    record.append(0)
                    record2.append(0)
                res['records'][analytic_account.name][a.code] = record
                if a.code in res['records_full']:
                    res['records_full'][a.code][2] += record2[2]
                    res['records_full'][a.code][4] += record2[4]
                else:
                    res['records_full'][a.code] = record2
                res['analytic_balance'] += analytic_balance
            if res['analytic_balance'] == 0:
                res['records'].pop(analytic_account.name)
            else:
                res['records'][analytic_account.name]['balance'] = \
                    result_parcial
            if analytic_account.name in res['records'].keys():
                res['records'][analytic_account.name]['code'] = \
                        analytic_account.code
            main_accounts = Account.search(['OR',
                ('code', 'in', codes),
            ])

    res['root'] = Analytic_Account.search([
        ('id', '=', data['analytic_account_root'])
    ])

    global_result = sum([a.balance for a in main_accounts]) + amount_profit
    global_result = global_result * (-1)

    res['global_result'] = global_result
    fiscalyear = Fiscalyear(data['fiscalyear'])
    if period:
        res['start_date'] = period.start_date
        res['end_date'] = period.end_date
    else:
        periods_dates = []
        for p in fiscalyear.periods:
            periods_dates.extend([p.start_date, p.end_date])
        res['start_date'] = min(periods_dates)
        res['end_date'] = max(periods_dates)
    res['period'] = period
    res['fiscalyear'] = fiscalyear
    res['analytic_lines'] = data['analytic_lines']
    res['detailed'] = data['detailed']
    res['without_balance'] = data['without_balance']

    return res


class PrintIncomeStatementCOLGAAPStart(ModelView):
    'Print Balance Sheet <Up>art'
    __name__ = 'analytic_account_co.print_income_statement_colgaap.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            help='Leave empty for all open fiscal year', required=True)
    posted = fields.Boolean('Posted Moves', help='Show posted moves only')
    start_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('type', '=', 'standard'),
        ], depends=['fiscalyear'], required=True)
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('type', '=', 'standard'),
        ], depends=['fiscalyear'], required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    detailed = fields.Boolean('Detailed')
    analytic_lines = fields.Boolean('Analytic Lines',
                                  states={'invisible': ~Eval('detailed'),
                                          },
                                  depends=['detailed'],
                                  help='Show account move detailed')
    without_balance = fields.Boolean('Without Balance')
    analytic_account_root = fields.Many2One(
        'analytic_account.account',
        'Analytic Account Root', required=True,
        domain=[('type', '=', 'root')]
    )
    analytic_account = fields.Many2Many(
        'analytic_account.account',
        None, None, 'Analytic Account',
        domain=[
            ('parent', '=', Eval(
                'analytic_account_root')),
            ('type', '!=', 'root')],
        depends=['analytic_account_root']
    )

    @staticmethod
    def default_posted():
        return False

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None

    @fields.depends('start_period')
    def on_change_start_period(self):
        if self.start_period:
            self.end_period = self.start_period.id


class PrintIncomeStatementCOLGAAP(Wizard):
    'Print Analytic Income Statement COLGAAP'
    __name__ = 'analytic_account_co.print_income_statement_colgaap'
    start = StateView(
        'analytic_account_co.print_income_statement_colgaap.start',
        'analytic_account_co.print_income_statement_colgaap_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('analytic_account_co.income_statement_colgaap')

    def do_print_(self, action):
        Period = Pool().get('account.period')
        Analytic_Account = Pool().get('analytic_account.account')

        ctx = {
            'fiscalyear': self.start.fiscalyear.id,
            'posted': self.start.posted,
            'cumulate': True,
        }

        periods = Period.search([
            ('start_date', '>=', self.start.start_period.start_date),
            ('end_date', '<=', self.start.end_period.end_date),
            ('type', '=', 'standard'),
        ])

        periods_ids = [p.id for p in periods]
        if self.start.analytic_account:
            analytic_account_ids = [
                ac.id for ac in self.start.analytic_account]
        else:
            analytic_account = Analytic_Account.search(
                [('parent.type', '=', 'root')])
            analytic_account_ids = [ac.id for ac in analytic_account]
        ctx['periods'] = periods_ids

        action['pyson_context'] = PYSONEncoder().encode(ctx)

        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'periods': periods_ids,
            'start_period': self.start.start_period.id,
            'end_period': self.start.end_period.id,
            'company': self.start.company.id,
            'detailed': self.start.detailed,
            'analytic_lines': self.start.analytic_lines,
            'posted': self.start.posted,
            'without_balance': self.start.without_balance,
            'analytic_account_root': self.start.analytic_account_root.id,
            'analytic_account': analytic_account_ids
        }
        return action, data

    def transition_print_(self):
        return 'end'


class IncomeStatementCOLGAAP(Report):
    __name__ = 'analytic_account_co.income_statement_colgaap'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(IncomeStatementCOLGAAP, cls).get_context(
            records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Period = pool.get('account.period')
        company = Company(data['company'])

        codes = ['4', '5', '6', '7']
        domain = [
            ('code', '>=', '4'),
        ]
        res = compute_report(data, domain, codes)

        report_context.update(res)
        report_context['start_period'] = Period(data['start_period'])
        report_context['end_period'] = Period(data['end_period'])
        report_context['company'] = company

        return report_context
